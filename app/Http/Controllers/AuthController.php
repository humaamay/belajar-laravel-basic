<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('register.index');
    }

    public function store(Request $request)
    {
        if (false == $request->filled(['firstname', 'lastname'])) {
            return redirect()->route('register');
        }

        return view('register.store', compact('request'));
    }
}
