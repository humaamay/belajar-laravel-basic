@extends('layouts.main')
@section('title', 'Form Sign Up')

@section('content')
      <h1>Buat Account Baru!</h1>
      <h2>Sign Up Form</h2>
      <form action="{{  @route('postRegister') }}" method="POST">
         {{ @csrf_field() }}
         <label for="firstname">First name:</label>
         <br/>
         <br/>
         <input type="text" name="firstname" id="firstname" required>
         <br/>
         <br/>
         <label for="lastname">Last name:</label>
         <br/>
         <br/>
         <input type="text" name="lastname" id="lastname" required>
         <br/>
         <br/>
         <label for="">Gender:</label>
         <br/>
         <br/>
         <input type="radio" name="gender" id="male" value="Male" required>
         <label for="male">Male</label>
         <br/>
         <input type="radio" name="gender" id="female" value="Female">
         <label for="female">Female</label>
         <br/>
         <input type="radio" name="gender" id="other" value="Other">
         <label for="other">Other</label>
         <br/>
         <br/>
         <label for="nationality">Nationality:</label>
         <br/>
         <br/>
         <select name="nationality" id="nationality" required>
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
         </select>
         <br/>
         <br/>
         <label for="language">Language Spoken:</label>
         <br/>
         <br/>
         <input type="checkbox" name="indonesia" id="indonesia">
         <label for="indonesia">Bahasa Indonesia</label>
         <br/>
         <input type="checkbox" name="english" id="english">
         <label for="english">English</label>
         <br/>
         <input type="checkbox" name="other" id="otherlanguage">
         <label for="otherlanguage">Other</label>
         <br/>
         <br/>
         <label for="bio">Bio:</label>
         <br/>
         <textarea name="bio" id="bio" cols="30" rows="10" required></textarea>
         <br/>
         <button type="submit">Sign Up</button>
      </form>
@endsection